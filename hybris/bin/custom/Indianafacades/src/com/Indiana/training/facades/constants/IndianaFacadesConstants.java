/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.Indiana.training.facades.constants;

/**
 * Global class for all IndianaFacades constants.
 */
@SuppressWarnings("PMD")
public class IndianaFacadesConstants extends GeneratedIndianaFacadesConstants
{
	public static final String EXTENSIONNAME = "Indianafacades";

	private IndianaFacadesConstants()
	{
		//empty
	}
}
