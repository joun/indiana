/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.Indiana.training.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.Indiana.training.fulfilmentprocess.constants.IndianaFulfilmentProcessConstants;

@SuppressWarnings("PMD")
public class IndianaFulfilmentProcessManager extends GeneratedIndianaFulfilmentProcessManager
{
	public static final IndianaFulfilmentProcessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (IndianaFulfilmentProcessManager) em.getExtension(IndianaFulfilmentProcessConstants.EXTENSIONNAME);
	}
	
}
